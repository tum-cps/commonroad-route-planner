
# Changelog
## [2022.11]
### Changed
- Add support for commonroad-io 2022.3
## [2022.8]
### Changed
- In addition to Scenario, accepts LaneletNetwork as the road network.
- In addition to PlanningProblem, accepts State and GoalRegion as the route planning problem.
## [2022.1]
- First release of the toolbox.